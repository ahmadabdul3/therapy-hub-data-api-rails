FactoryGirl.define do
  factory :patient_invitation do
    email "MyText"
    therapist nil
  end
  factory :assignment do
    patient nil
    therapist nil
    note "MyText"
    context "MyText"
    metadata ""
    deleted false
  end
  factory :review do
    patient nil
    therapist nil
    title "MyText"
    body "MyText"
    star_rating 1
  end
  factory :appointment do
    user nil
    therapist nil
    day "MyText"
    number "MyText"
    month "MyText"
    year "MyText"
  end
  factory :patient do
    therapist nil
  end
  factory :schedule_entry do
    day "MyText"
    regularity "MyText"
    start_date "2017-08-14 11:54:51"
    end_date "2017-08-14 11:54:51"
    therapist nil
    start_time "MyText"
    start_time_period_of_day "MyText"
    end_time "MyText"
    end_time_period_of_day "MyText"
    month "MyText"
    day_number "MyText"
    year "MyText"
  end
  factory :therapist do
    user nil
  end
  factory :timeline_entry do
    user nil
    log ""
  end
  factory :user_emotional_catalyst do
    user nil
    emotional_catalyst nil
  end
  factory :emotional_catalyst do
    text "MyText"
    sentiment "MyText"
  end
  factory :goal do
    text "MyText"
    status "MyText"
  end
  factory :status do
    feeling "MyText"
    thoughts "MyText"
    latest_for_day false
  end
  factory :user do
    first_name "MyText"
    last_name "MyText"
    profile_photo_url "MyText"
    dob "2017-07-26"
    email "MyText"
  end
end
