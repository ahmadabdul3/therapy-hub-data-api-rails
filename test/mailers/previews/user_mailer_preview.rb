# frozen_string_literal: true

class UserMailerPreview < ActionMailer::Preview
  def trial_end_notification_email
    UserMailer.trial_end_notification_email(User.first)
  end

  def appointment_charge_email
    UserMailer.appointment_charge_email(User.first)
  end

  def weekly_text_invoice_email
    UserMailer.weekly_text_invoice_email(User.first)
  end
end
