Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      #
      # try to keep all urls with dashes instead of underscores

      namespace :stripe_api do
        #######################
        ### CUSTOMER ROUTES ###
        #######################
        # this is created as a custom route to keep urls with dashes instead of underscores
        post '/customers', to: 'customers#create', as: 'customers'

        ######################
        ### WEKBOOK ROUTES ###
        ######################
        # this is created as a custom route to keep urls with dashes instead of underscores
        post '/webhook-events', to: 'webhook_events#capture_event', as: 'webhook_events/capture_event'

        ##############################
        ### PAYMENT SOURCES ROUTES ###
        ##############################
        # this is created as a custom route to keep urls with dashes instead of underscores
        post '/payment-sources', to: 'payment_sources#create', as: 'payment_sources'

        ####################################
        ### WEEKLY TEXT NO TRIALS ROUTES ###
        ####################################
        # this is created as a custom route to keep urls with dashes instead of underscores
        post '/weekly-text-no-trials', to: 'weekly_text_no_trials#create', as: 'weekly_text_no_trials'

        #################################
        ### APPOINTMENT CHARGE ROUTES ###
        #################################
        # this is created as a custom route to keep urls with dashes instead of underscores
        post '/appointment-charges', to: 'appointment_charges#create', as: 'appointment_charges'

        ###########################
        ### SUBSCRIPTION ROUTES ###
        ###########################
        resources :subscriptions, only: [:destroy]

      end # stripe_api

      ###################
      ### AUTH ROUTES ###
      ###################
      # this is created as a custom route to keep urls with dashes instead of underscores
      post '/auths/determine-auth-type', to: 'auths#determine_auth_type', as: 'auth_type'

      ###################
      ### USER ROUTES ###
      ###################
      post '/users/get-by-auth0-id', to: 'users#get_by_auth0_id', as: 'get_by_auth0_id'
      post '/users/get-therapist-for-user', to: 'users#get_therapist_for_user', as: 'get_therapist_for_user'
      post '/users/regular-update', to: 'users#regular_update', as: 'regular_user_update'
      patch '/:auth0_id/users/:id', to: 'users#ios_device_token_update', as: 'ios_device_token_update'

      #######################################
      ### USER WITH STRIPE ACCOUNT ROUTES ###
      #######################################
      post '/users-with-stripe-accounts', to: 'users_with_stripe_accounts#create', as: 'users_with_stripe_accounts'

      ##########################
      ### APPOINTMENT ROUTES ###
      ##########################
      resources :appointments, only: [:index, :create, :destroy, :update]
      get '/appointments/next-for-therapist/:therapist_id', to: 'appointments#next_for_therapist', as: 'next_appointment_for_therapist'
      post '/appointments/therapist-create-for-patient', to: 'appointments#therapist_create_for_patient', as: 'therapist_create_appointment_for_patient'

      #############################
      ### [DAILY] STATUS ROUTES ###
      #############################
      resources :statuses, only: [:create]
      get '/statuses/latest-today', to: 'statuses#latest_today', as: 'latest_today'

      ########################
      ### HOME PAGE ROUTES ###
      ########################
      get '/home-pages/generate-data', to: 'home_pages#generate_home_page_data', as: 'generate_home_page_data'

      #############################
      ### OBJECTIVE PAGE ROUTES ###
      #############################
      get '/objective-pages/generate-data', to: 'objective_pages#generate_objective_page_data', as: 'generate_objective_page_data'

      ###################
      ### GOAL ROUTES ###
      ###################
      resources :goals, only: [:create, :update]
      get '/goals/in-progress', to: 'goals#get_in_progress', as: 'get_in_progress_goals'
      get '/goals/historical', to: 'goals#get_historical', as: 'get_historical_goals'

      #########################
      ### HAPPY ITEM ROUTES ###
      #########################
      # this is created as a custom route to keep urls with dashes instead of underscores
      post '/happy-items', to: 'happy_items#create', as: 'happy_items'
      delete '/happy-items/:id', to: 'happy_items#destroy', as: 'happy_item'
      get '/happy-items/all-with-suggestions', to: 'happy_items#get_all_with_suggestions', as: 'get_happy_items_with_suggestions'

      ###########################
      ### UNHAPPY ITEM ROUTES ###
      ###########################
      # this is created as a custom route to keep urls with dashes instead of underscores
      post '/unhappy-items', to: 'unhappy_items#create', as: 'unhappy_items'
      delete '/unhappy-items/:id', to: 'unhappy_items#destroy', as: 'unhappy_item'
      get '/unhappy-items/all-with-suggestions', to: 'unhappy_items#get_all_with_suggestions', as: 'get_unhappy_items_with_suggestions'

      #############################
      ### TIMELINE ENTRY ROUTES ###
      #############################
      get '/timeline-entries', to: 'timeline_entries#index', as: 'timeline_entries'
      get '/timeline-entries/for-user/:user_id', to: 'timeline_entries#show_for_user', as: 'timeline_entry_show_for_user'
      patch '/timeline-entries/update-log', to: 'timeline_entries#update_log', as: 'timeline_entries_update_log'

      #############################
      ### SCHEDULE ENTRY ROUTES ###
      #############################
      get '/schedule-entries', to: 'schedule_entries#index', as: 'schedule_entries'
      post '/schedule-entries', to: 'schedule_entries#create', as: 'schedule_entries_create'
      delete '/schedule-entries', to: 'schedule_entries#destroy', as: 'schedule_entries_destroy'

      #####################################
      ### THERAPIST AVAILABILITY ROUTES ###
      #####################################
      get '/therapist-availabilities', to: 'therapist_availability#index', as: 'therapist_availabilities'

      ######################
      ### PATIENT ROUTES ###
      ######################
      resources :patients, only: [:index]

      ########################
      ### THERAPIST ROUTES ###
      ########################
      resources :therapists, only: [:update]

      #####################
      ### REVIEW ROUTES ###
      #####################
      resources :reviews, only: [:index, :create]

      #########################
      ### ASSIGNMENT ROUTES ###
      #########################
      resources :assignments, only: [:index, :create, :update]
      get '/assignments/for-user/:user_id', to: 'assignments#for_user', as: 'assignments_for_user'

      #################################
      ### PATIENT INVITATION ROUTES ###
      #################################
      post '/patient-invitations', to: 'patient_invitations#create', as: 'patient_invitations_create'

    end # v1
  end # api
end
