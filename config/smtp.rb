SMTP_SETTINGS = {
  :address            => 'smtp-relay.gmail.com',
  :port               => 587,
  :domain             => 'gmail.com',
  :authentication     => :plain,
  :user_name          => 'info@therapyhubapp.com',
  :password           => ENV['INFO_EMAIL_PASS'],
  :enable_starttls_auto => true
}

if ENV["EMAIL_RECIPIENTS"].present?
  Mail.register_interceptor RecipientInterceptor.new(ENV["EMAIL_RECIPIENTS"])
end
