# frozen_string_literal: true

class UserMailer < ApplicationMailer
  default from: 'info@therapyhubapp.com'

  def trial_end_notification_email(user)
    @user_name = user.first_name
    mail(to: user.email, subject: 'TherapyHub trial - 3 days left')
  end

  def appointment_charge_email(user)
    @user_name = user.first_name
    mail(to: user.email, subject: 'TherapyHub video session invoice')
  end

  def weekly_text_invoice_email(user)
    @user_name = user.first_name
    mail(to: user.email, subject: 'TherapyHub weekly text invoice')
  end
end
