# frozen_string_literal: true
require 'jwt'

class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception
  before_action :prep_data

  # - this will not work on relations/models
  # - it's probably better to return model.attributes
  #   instead of the model itself when rendering json:
  def camelize(hash)
    SnakeToCamelCaseTransformer.new(hash).transform
  end

  private

  def prep_data
    token = find_http_token
    raise JWT::VerificationError unless token

    token = token.split(' ').last
    cached_jwks_hash = JwksHashCache.get_hash
    cached_jwks_hash = JwksHashCache.update_hash(get_jwks_hash) unless cached_jwks_hash

    decoded_token = verify(token, cached_jwks_hash)
    user_data = decoded_token[0]
    @auth0_id = user_data["sub"]
    @email = user_data["email"]
    @name = user_data["name"]
    @role = user_data["role"]
    @city = user_data["city"]
    @state = user_data["state"]
    @degree = user_data["degree"]
    @code = user_data["code"]

    @user = User.find_by_auth0_id(@auth0_id)
    setup_patient_therapist if @user
  rescue JWT::VerificationError, JWT::DecodeError => e
    render json: { message: 'Not Authenticated' }, status: :unauthorized
  end

  def find_http_token
    # - using stripe, we can't add headers unless we intercept the post request
    #   and I dont want to deal with that, so we will get the jwt token from
    #   the params instead - only for payments
    request.headers['HTTP_JWT_TOKEN'] || request.params.dig('stripe_customer', 'HTTP_JWT_TOKEN') || request.params.dig('payment_source', 'HTTP_JWT_TOKEN')
  end

  def setup_patient_therapist
    if @user.is_patient
      @patient = @user.patient || nil
      @therapist = @patient.therapist || nil
    elsif @user.is_therapist
      @therapist = @user.therapist || nil
    end
  end

  def verify(token, jwks_hash)
    JWT.decode(token, nil,
               true, # Verify the signature of this token
               algorithm: 'RS256',
               iss: "https://#{ENV["AUTH0_DOMAIN"]}/",
               verify_iss: true,
               aud: ENV["AUTH0_API_AUDIENCE"],
               verify_aud: true) do |header|
      jwks_hash[header['kid']]
    end
  end

  def get_jwks_hash
    jwks_raw = Net::HTTP.get URI("https://#{ENV["AUTH0_DOMAIN"]}/.well-known/jwks.json")
    jwks_keys = Array(JSON.parse(jwks_raw)['keys'])
    Hash[
      jwks_keys
      .map do |k|
        [
          k['kid'],
          OpenSSL::X509::Certificate.new(
            Base64.decode64(k['x5c'].first)
          ).public_key
        ]
      end
    ]
  end
end
