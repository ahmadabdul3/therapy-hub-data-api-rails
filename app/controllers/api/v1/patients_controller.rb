# frozen_string_literal: true

module Api
  module V1
    class PatientsController < ApplicationController
      def index
        render json: camelize({ message: 'success', patients: patients })
      end

      private

      def valid_params
        params.require(:patient).permit(
          :id
        )
      end

      def patients
        @therapist.patients.map do |patient|
          patient.attributes.merge({ user: patient.user.attributes })
        end
      end
    end
  end
end
