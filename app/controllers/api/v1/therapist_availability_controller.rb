# frozen_string_literal: true

module Api
  module V1
    class TherapistAvailabilityController < ApplicationController
      # - this controller has to get therapist schedule entries
      #   and generate some availability times
      #
      # - for example: if a therapist has a schedule entry like the following
      #   - monday, 1p - pm, repeating -
      #   then, we have to return a list of mondays from today
      #   until 2 months from now (the number of months can change)
      #   each with a time block of 1pm - 2pm
      #
      # - but thats a simple case, in reality, there may
      #   be 2 or 3 monday availabilities, each with a different time frame
      #
      # - we need a service that's smart enough to generate the list correctly
      #   and account for every schedule entry for any day of the week
      #
      # - and it also has to account for possible non-repeating schedule entries
      #   because there could be one-timer/ad-hoc schedule openings
      def index
        render json: camelize({
          message: 'gud',
          availability: TherapistAvailabilityGenerator.new(@user).generate
        })
      end

      private

      def valid_params
        params.require(:therapist_availability).permit(
          :id
        )
      end

      def therapist
        @user.therapist
      end
    end
  end
end
