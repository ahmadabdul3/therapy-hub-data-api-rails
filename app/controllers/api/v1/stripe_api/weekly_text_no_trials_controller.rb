# frozen_string_literal: true

module Api
  module V1
    module StripeApi
      class WeeklyTextNoTrialsController < ApplicationController
        def create
          card_source = valid_params[:stripeToken]
          @user.subscribe_to_weekly_text(card_source)

          render json: camelize({
            status: 'success',
            user: @user.attributes,
            therapist: @user.patient.therapist.user.attributes
          })
        end

        private

        def valid_params
          params.require(:stripe_customer).permit(
            :HTTP_JWT_TOKEN,
            :stripeToken,
            :stripeTokenType,
            :stripeEmail,
          )
        end
      end
    end
  end
end
