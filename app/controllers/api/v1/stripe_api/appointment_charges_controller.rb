# frozen_string_literal: true

require "stripe"

module Api
  module V1
    module StripeApi
      class AppointmentChargesController < ApplicationController
        def create
          Stripe.api_key = ENV['STRIPE_API_KEY']

          appointment = Appointment.find(valid_params[:appointment_id])

          if !appointment.is_paid
            Stripe::Charge.create(
              :amount => 2500,
              :currency => "usd",
              :customer => @user.stripe_customer_id,
              :description => "Video session for #{@user.email}"
            )

            appointment.update_attributes(is_paid: true, is_active: false)
          end

          # render json: { status: 'fail' }
          render json: { status: 'success' }
        rescue Stripe::CardError => e
          # Since it's a decline, Stripe::CardError will be caught
          body = e.json_body
          err = body[:error]
          message = err[:message] || 'Error'
          render json: { status: 'fail', message: message }
        rescue => e
          # Something else happened, completely unrelated to Stripe
          puts e
          message = 'Something went wrong, please try again'
          render json: { status: 'fail', message: message }
        end

        private

        def valid_params
          params.require(:appointment_charge).permit(
            :appointment_id
          )
        end
      end
    end
  end
end
