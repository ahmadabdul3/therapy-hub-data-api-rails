# frozen_string_literal: true

# require "stripe"

module Api
  module V1
    module StripeApi
      class WebhookEventsController < ApplicationController
        skip_before_action :prep_data

        def capture_event
          customer_id = valid_params[:data][:object][:customer]
          @user = User.find_by_stripe_customer_id(customer_id)
          handle_event if @user

          render json: { status: 'success' }
        end

        private

        def valid_params
          params.permit!
        end

        def event_types
          {
            trial_end: 'customer.subscription.trial_will_end',
            invoice_updated: 'invoice.updated',
            customer_subscription_deleted: 'customer.subscription.deleted',
            charge_succeeded: 'charge.succeeded',
          }
        end

        def handle_event
          event_type = valid_params[:type]
          if event_type == event_types[:trial_end]
            UserMailer.trial_end_notification_email(@user).deliver_now
          elsif event_type == event_types[:invoice_updated]
            handle_invoice_updated
          elsif event_type == event_types[:customer_subscription_deleted]
            handle_subscription_deleted
          elsif event_type == event_types[:charge_succeeded]
            handle_charge_succeeded
          end
        end

        def handle_invoice_updated
          paid = valid_params[:data][:object][:paid]
          if paid
            return if @user.hasPaidBillingStatus
            @user.assignPaidBillingStatus
            return
          end
          @user.revert_to_free_account
        end

        def handle_subscription_deleted
          @user.revert_to_free_account
        end

        def handle_charge_succeeded
          amount = valid_params[:data][:object][:amount]
          UserMailer.appointment_charge_email(@user).deliver_now if amount == 2500
          UserMailer.weekly_text_invoice_email(@user).deliver_now if amount == 2000
        end
      end
    end
  end
end
