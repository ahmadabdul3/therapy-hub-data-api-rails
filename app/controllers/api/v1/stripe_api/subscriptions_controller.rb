# frozen_string_literal: true

# - we should maybe move the weekly text no trial controller code into here
# = should be handled by subscriptions controller create action, with a param
#   for what kind of subscription
require "stripe"
Stripe.api_key = ENV['STRIPE_API_KEY']

module Api
  module V1
    module StripeApi
      class SubscriptionsController < ApplicationController
        def create

        end

        def destroy
          @stripe_customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
          @subscription = Stripe::Subscription.retrieve(customer_subscription_id)

          delete_subscription if @subscription

          render json: camelize({
            status: 'success',
            message: 'deleted subscription',
            user: @user.get_attributes,
          })
        end

        private

        # - no params for now, we know the user
        #   from the auth token, and we just remove the
        #   subscription for that user
        def valid_params
          params.require(:subscription)
        end

        def delete_subscription
          at_period_end = false
          at_period_end = true if @user.created_at > 3.days.ago
          result = @subscription.delete(at_period_end: at_period_end)
          # - parse the period end time into a rails DateTime object
          period_end = Time.at(result[:current_period_end]).to_datetime
          @user.update_attributes(
            subscription_end_time: period_end,
            billing_status: determine_billing_status
          )
        end

        def customer_subscription_id
          subscriptions = @stripe_customer[:subscriptions][:data]
          return subscriptions[0][:id] if subscriptions.any?
        end

        def determine_billing_status
          if @user.billing_status == 'weekly-text'
            return 'weekly-text-cancelled'
          elsif @user.billing_status == 'weekly-text-trial-with-card'
            return 'weekly-text-trial-with-card-cancelled'
          end
        end
      end
    end
  end
end
