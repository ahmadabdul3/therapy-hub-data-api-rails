# frozen_string_literal: true

# require "stripe"

module Api
  module V1
    module StripeApi
      class PaymentSourcesController < ApplicationController

        # - this controller should only be hit when 'pay bill' is used on the front-end
        # - if the user is already a 'weekly-text' customer
        #   we just update their stripe customer record with the payment source
        # - if the user is a 'free' customer, it means they
        #   already used the trial and it ran out (but we can check user.trial_used to be sure),
        #   and  we enroll them in a weekly-text plan, but without the trial
        def create
          Stripe.api_key = ENV['STRIPE_API_KEY']
          stripe_customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
          stripe_customer.source = valid_params[:stripeToken]
          stripe_customer.save

          # - weekly text trial doesnt mean they're still in the trial period, it just
          #   means they're in the plan that has a trial and they've added a card
          trial_card = 'weekly-text-trial-with-card'
          trial_no_card = 'weekly-text-trial-no-card'
          @user.update_attributes(billing_status: trial_card) if @user.billing_status == trial_no_card

          render json: { status: 'success' }
        end

        private

        def valid_params
          params.require(:payment_source).permit(
            :HTTP_JWT_TOKEN,
            :stripeToken,
            :stripeTokenType,
            :stripeEmail,
          )
        end
      end
    end
  end
end
