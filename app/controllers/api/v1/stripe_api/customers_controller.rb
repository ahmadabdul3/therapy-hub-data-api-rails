# frozen_string_literal: true

# require "stripe"

module Api
  module V1
    module StripeApi
      class CustomersController < ApplicationController
        def create
          
        end

        private

        def valid_params
          params.require(:stripe_customer).permit(
            :HTTP_JWT_TOKEN,
            :stripeToken,
            :stripeTokenType,
            :stripeEmail,
          )
        end
      end
    end
  end
end
