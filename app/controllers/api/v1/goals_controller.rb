# frozen_string_literal: true

module Api
  module V1
    class GoalsController < ApplicationController

      def create
        goal_params = valid_params.merge({ user: @user })
        goal = Goal.new(goal_params)

        if goal.save
          # - gonna do this through a requests to the timeline controller
          #   so that the timeline updates on the front-end
          ### Timeline::LogMutator.new(@user, add_goal_log_entry).mutate
          render json: { message: 'success', goal: goal }
        else
          # no validation yet, so dont know which status to send back
          # will depend on error type
          render json: { message: 'failure' }
        end
      end

      # - it would probably be clearer to have 2 separate methods for 
      #   updating a goal; accomplish and abandon
      # - or maybe not... need to think about it more
      def update
        @goal = Goal.find_by_id(params[:id])

        if @goal.update_attributes(valid_params)
          complete_assignment if @goal.assignment_id && @goal.is_done?
          render json: camelize({ message: 'success', goal: @goal.attributes })
        else
          render json: { message: 'update failure' }
        end
      end

      # - technically this should be moved to a better name
      #   but I'm sticking suggested goals into here
      #   because the page that needs to get in progress goals
      #   also needs to render suggested goals
      def get_in_progress
        goals = @user.in_progress_goals.map do |goal|
          camelize(goal.attributes)
        end

        # - suggested goals are any goal other than what the user is
        #   currently tracking
        # - they can be historical goals for this user, other users,
        #   or in progress goals for other users
        suggestions = Goal.suggested(goals.pluck(:text))
        render json: { message: 'sucess', goals: goals, suggestions: suggestions }
      end

      def get_historical
        goals = @user.historical_goals
        render json: { message: 'sucess', goals: goals }
      end

      private

      def valid_params
        params.require(:goal).permit(
          :id,
          :text,
          :status,
          :assignment_id
        )
      end

      def add_goal_log_entry
        { goals: valid_params[:text] }
      end

      def complete_assignment
        @goal.assignment.update_attributes({ status: 'complete' })
      end
    end
  end
end
