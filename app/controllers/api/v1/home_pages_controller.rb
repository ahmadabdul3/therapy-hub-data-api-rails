# frozen_string_literal: true

module Api
  module V1
    class HomePagesController < ApplicationController
      # - get all the data related to the home page
      #
      # - for now, this includes:
      #   1. the latest daily status
      #
      # - in the future:
      #   2. the nearest appointment (have to build model and logic for this)
      #   3. the stage of change
      #   4. other stuff...
      def generate_home_page_data
        data = HomePageDataGenerator.new(@user).generate
        render json: camelize({ message: 'success', data: data })
      end

      private

      # no 'valid params' yet
      # def valid_params
      #   params.require(:something).permit()
      # end
    end
  end
end
