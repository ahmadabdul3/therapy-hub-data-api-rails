# frozen_string_literal: true

module Api
  module V1
    class UnhappyItemsController < ApplicationController
      def create
        create_emotional_catalyst unless emotional_catalyst_exists?
        if join_relationship_exists?
          activate_join_relationship if join_relationship_disabled?
          render json: camelize({
            message: 'already exists',
            unhappy_item: @user_emotional_catalyst.emotional_catalyst
          })
        else
          create_join_relationship
          render json: camelize({
            message: 'success',
            unhappy_item: @emotional_catalyst
          })
        end
      end

      def get_all_with_suggestions
        render json: camelize({
          message: 'success',
          unhappy_items: @user.unhappy_items,
          suggestions: EmotionalCatalyst.suggested_unhappy(@user.unhappy_items.pluck(:text)),
        })
      end

      def destroy
        happy_item = UserEmotionalCatalyst.where(user_id: @user.id, emotional_catalyst_id: params[:id]).first
        happy_item.update_attributes(status: 'deleted')
        render json: { message: 'success' }
      end

      private

      def valid_params
        params.require(:unhappy_item).permit(
          :id,
          :text,
        )
      end

      def catalyst_params
        valid_params.merge({ sentiment: 'negative' })
      end

      def create_emotional_catalyst
        @emotional_catalyst = EmotionalCatalyst.new(catalyst_params)
        @emotional_catalyst.save
      end

      def emotional_catalyst_exists?
        @emotional_catalyst ||= EmotionalCatalyst.where(
                                  'lower(text) = ?', valid_params[:text].downcase
                                ).where(sentiment: 'negative')
                                .first
      end

      def create_join_relationship
        @userEmotionalCatalyst = UserEmotionalCatalyst.new({ user: @user, emotional_catalyst: @emotional_catalyst })
        @userEmotionalCatalyst.save
      end

      def join_relationship_exists?
        @user_emotional_catalyst ||= UserEmotionalCatalyst.where(user: @user, emotional_catalyst: @emotional_catalyst).first
      end

      def activate_join_relationship
        @user_emotional_catalyst.update_attributes(status: 'active')
      end

      def join_relationship_disabled?
        @user_emotional_catalyst.status == 'deleted'
      end
    end
  end
end
