# frozen_string_literal: true

module Api
  module V1
    class UsersWithStripeAccountsController < ApplicationController
      skip_before_action :prep_data, only: [:create]

      def create
        @user = User.get_new(user_params, {})
        if @user.save
          plan = PatientBilling::WeeklyTextWithTrial.new(
            user: @user, card_source: valid_params[:stripeToken])
          plan.subscribe
          @user.match_to_therapist
          render json: camelize({ status: 'success' })
        else
          render json: camelize({ status: 'fail' })
        end
      end

      private

      def valid_params
        # - stripe token type and stripe email
        #   are being passed into here but not used for now
        params.require(:user_with_stripe_account).permit!
        # (
        #   :email,
        #   user_metadata: [
        #     :first_name_nickname,
        #     :state,
        #   ],
        #   :id,
        #   :stripeToken,
        #   :stripeTokenType,
        #   :stripeEmail,
        # )
      end

      def user_params
        {
          email: valid_params[:email],
          first_name: valid_params[:user_metadata][:first_name_nickname],
          state: valid_params[:user_metadata][:state],
          auth0_id: "auth0|#{valid_params[:id]}",
          roles: ['patient']
        }
      end
    end
  end
end
