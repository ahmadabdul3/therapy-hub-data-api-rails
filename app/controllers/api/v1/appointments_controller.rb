# frozen_string_literal: true

module Api
  module V1
    class AppointmentsController < ApplicationController
      def index
        appointments = get_based_on_role

        render json: camelize({ message: 'success', appointments: appointments })
      end

      def create
        # - we need to make sure that only 1 appointment at a time
        #   and we need to track when an appointment time passed or if the appointment
        #   was attended
        # - the hard way to do this is to add some sort of 'appointment_attended'
        #   field and check that. If the patient.appointments.where.not(appointment_attended)
        #   - the reason this is hard is because we have to update other places
        #     to make sure this field is updated
        # - the easy way is to get all appointments where appointment time
        # if @user.patient.appointments.where(is_active: true).count > 0
        #   render json: { message: 'user already has appointment', status: 'fail' }
        # else
        appointment = new_appointment(true, true)
        if appointment.save
          apt = appointment.attributes
          render json: camelize({ status: 'success', message: 'success', appointment: apt })
        else
          render json: { status: 'fail', message: 'appointment create fail' }
        end
        # end
      end

      def therapist_create_for_patient
        appointment = new_appointment(false, true)
        if appointment.save
          render json: camelize({ message: 'success', appointment: appointment.attributes })
        else
          render json: { message: 'appointment create fail' }
        end
      end

      def destroy
        if Appointment.find(params[:id]).update_attributes(is_deleted: true, is_active: false)
          render json: { message: 'success' }
        else
          render json: { message: 'appointment destroy failed' }
        end
      end

      def next_for_therapist
        therapist = Therapist.find(params[:therapist_id])
        appointment = therapist.next_appointment
        render json: camelize({ status: 'success', appointment: appointment })
      end

      def update
        if Appointment.find(params[:id]).update_attributes(valid_params)
          render json: { message: 'success' }
        else
          render json: { message: 'appointment update failed' }
        end
      end

      private

      def valid_params
        params.require(:appointment).permit(
          :id,
          :therapist_id,
          :patient_id,
          :day,
          :number,
          :month,
          :year,
          :start_time,
          :start_time_minute,
          :start_time_period_of_day,
          :end_time,
          :end_time_minute,
          :end_time_period_of_day,
          :started_by_therapist
        )
      end

      def new_appointment(include_patient, include_therapist)
        new_params = valid_params
        # - this used to be new_params.merge({ patient: @patient }) ...
        #   but that wasnt working for some retarded reason
        #   so it's staying like this for now
        # - When a regular appointment is created, we dont have
        #   the ids of the patient or therapist, just the user, so we have
        #   to add them to the appointment record
        # - but when a therapist creates an appointment for the patient, the patient id is sent
        #   with the request so its automatically added to the record as part of the params
        #   so we just have to add the therapist id
        appointment = Appointment.new(new_params)
        appointment.patient = @patient if include_patient
        appointment.therapist = @therapist if include_therapist
        appointment
      end

      def get_based_on_role
        if @user.is_patient
          @patient.all_appointments
        elsif @user.is_therapist
          @therapist.all_appointments
        end
      end
    end
  end
end
