# frozen_string_literal: true

module Api
  module V1
    class ReviewsController < ApplicationController
      # - for now, reviews are always in context of the therapist
      #
      # - in other words, we're not going to fetch all reviews for a patient
      #   only all reviews for a therapist
      def index
        render json: camelize({ message: 'success', reviews: reviews })
      end

      def create
        references = { patient: @patient, therapist: @therapist }
        review = Review.new(valid_params.merge(references))
        if review.save
          render json: camelize({ message: 'success', review: review.attributes })
        else
          render json: { message: 'fail' }
        end
      end

      private

      def valid_params
        params.require(:review).permit(
          :id,
          :title,
          :body,
          :star_rating,
        )
      end

      def reviews
        # - this will fail if the patient isnt matched to a therapist because
        #   because it will try to go to the next line and return @user.therapist.reviews
        #   but that fails because the user is a patient
        # - so the next line has to check @user.is_therapist to make sure
        #   if the user is a patient but isnt matched, it just returns nil
        # - should also just check on the front-end and not do a server request
        #   if there's no therapist for the user
        return @user.patient.therapist.reviews if @user.is_patient && @user.patient.therapist
        @user.therapist.reviews if @user.is_therapist
      end
    end
  end
end
