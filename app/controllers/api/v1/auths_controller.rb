# frozen_string_literal: true

# - since we're using auth0 for authentication
#   it just returns an id, doesnt tell us if it was
#   a login or signup action
#   so we have to check if the returned UUID exists or not
#
# - if it exists, it was login, so we return the user data
#   and we also return "login" as the action type
#
# - otherwise it was signup and we create a user before
#   returning data, and we return "signup" as the action type
#
# - the action types are returned because the front-end
#   has a different flow for sign up

# *** IMPORTANT ***
# - the create user method needs to create a therapist record
#   if the user's role is 'therapist'
#   and in the future, a patient record if the role is 'patient'

module Api
  module V1
    class AuthsController < ApplicationController

      def determine_auth_type
        # this should probably handle exceptions instead of if/else
        # will update
        if user_exists?
          render json: camelize({
            action_type: 'login',
            user: @user.get_attributes
          })
        elsif create_user
          send_hash = {
            action_type: 'signup',
            user: @user.get_attributes
          }

          send_hash[:therapist] = @user.get_therapist_for_patient
          render json: camelize(send_hash)
        else
          render json: camelize({ message: 'user create failed' }), status: :bad_request
        end
      end

      private

      # - should rename 'role' to something else
      #   like where the request is coming from
      # - the idea is to know which role to assign to the user
      # - and that's determined by which front-end location
      #   triggered the authentication request
      # - in the future we can use rules in auth0 to tell us
      def valid_params
        params.require(:auth).permit(
          :role,
        )
      end

      # can probably get rid of this soon
      # because the user is set up in the application controller
      def user_exists?
        @user = User.find_by_auth0_id(@auth0_id)
      end

      # - this just puts the role into an array
      #   because the user model's 'roles' field is an array
      def user_params
        role = @role || 'patient'
        params = {
          auth0_id: @auth0_id,
          email: @email,
          first_name: @name,
          roles: [role],
          city: @city,
          state: @state,
          code: @code,
        }

        params[:billing_status] = 'therapist' if role == 'therapist'
        params
      end

      def create_user
        @user = User.get_new(user_params, {
          degree: @degree, code: @code
        })
        @user.save
      end
    end
  end
end
