# frozen_string_literal: true

module Api
  module V1
    class StatusesController < ApplicationController
      # check if there is already a status for today
      # if so, make it not the latest for today
      def create
        mark_status_as_old if status_for_today_exists?
        status_params = valid_params.merge({ user: @user })
        status = Status.new(status_params)

        if status.save
          response = { message: 'success', status: status.attributes }
          if status_assignment_exists?
            mark_assignment_as_complete 
            response[:assignment_complete] = true
          end

          render json: camelize(response)
        else
          # no validation yet, so dont know which status to send back
          # will depend on error type
          render json: { message: 'failure' }
        end
      end

      def latest_today
        if status_for_today_exists?
          render json: { message: 'success', status: @existing_status_for_today }
        else
          render json: { message: 'no status today' }
        end
      end

      private

      def valid_params
        params.require(:status).permit(
          :id,
          :feeling,
          :thoughts
        )
      end

      def status_for_today_exists?
        @existing_status_for_today ||= Status.latest_today(@user.id).first
      end

      def mark_status_as_old
        @existing_status_for_today.update_attributes(latest_for_day: false)
      end

      def status_assignment_exists?
        # - this is a patient user
        @assignment ||= @user.patient.assignments.where(context: 'Submit daily status', status: 'in-progress').first
      end

      def mark_assignment_as_complete
        @assignment.update_attributes(status: 'complete')
      end
    end
  end
end
