# frozen_string_literal: true

module Api
  module V1
    class PatientInvitationsController < ApplicationController
      def create
        if invitation_exists?
          render json: camelize({
            status: 'fail',
            message: 'An invitation for that email already exists'
          })
        else
          invitation = PatientInvitation.new(valid_params)
          invitation.email = invitation.email.downcase
          if invitation.save
            render json: camelize({
              status: 'success',
            })
          else 
            render json: camelize({
              status: 'fail',
              message: 'An error occured, please try again'
            })
          end
        end
      end

      private

      def valid_params
        params.require(:patient_invitation).permit(
          :id,
          :email,
          :therapist_id
        )
      end

      def invitation_exists?
        PatientInvitation.find_by_email(valid_params[:email])
      end
    end
  end
end
