# frozen_string_literal: true

module Api
  module V1
    class ScheduleEntriesController < ApplicationController
      def index
        entries = therapist.schedule_entries.map do |entry|
          camelize(entry.attributes)
        end
        render json: { message: 'success', entries: entries }
      end

      # - a therapist may create multiple schedule entries for the same day
      #   but for different time frames
      #
      # - in these situations, we should check for schedule entries that
      #   have the same day AND the same regularity
      #   (because if its a one time event it should be separate)
      #
      # I DONT THINK I'M DOING THE BELOW COMMENT --- JUST CREATING A NEW ENTRY
      # - *** if we have a matching record, we just append to its time frames array
      #   instead of creating a new entry ***
      # 
      def create
        entry = ScheduleEntry.unique(valid_params.merge(therapist: therapist), therapist.id)
        if entry.save
          render json: camelize({
            message: 'success',
            entry: entry.attributes,
          })
        else
          render json: { message: 'fail' }
        end

      rescue ScheduleEntry::DuplicateScheduleEntry
        render json: { message: 'overlapping event times' }, status: :conflict
      end

      def destroy
        if ScheduleEntry.destroy(valid_params[:id])
          render json: { message: 'success' }
        else
          render json: { message: 'fail' }
        end
      end

      private

      def therapist
        @user.therapist
      end

      def valid_params
        params.require(:schedule_entry).permit(
          :id,
          :regularity,
          :day,
          :start_date,
          :end_date,
          :start_time,
          :end_time,
          :start_time_minute,
          :end_time_minute,
          :start_time_period_of_day,
          :end_time_period_of_day,
          :month,
          :year,
          :day_number,
        )
      end
    end
  end
end
