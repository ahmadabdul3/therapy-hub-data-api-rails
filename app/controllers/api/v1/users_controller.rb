# frozen_string_literal: true

module Api
  module V1
    class UsersController < ApplicationController
      skip_before_action :prep_data, only: [:ios_device_token_update]

      def create

      end

      def get_by_auth0_id
        # - this needs to be updated to just use the @user
        #   created in the application controller on every
        #   API request. Leaving for now cuz no time to test it
        #   after changing it.
        # - since this action is called everytime someone comes to the app
        #   we have to handle something important here, but its done in a
        #   very hacky way.
        #   - basically, we need to clear out any appointments that were
        #     not acted upon by the patients, otherwise the patients cant
        #     schedule any new appointments
        #   - the reason is, only 1 appointment is allowed at a time right now
        #     and appointments to the front-end are limited to yesterday and up
        #   - so if a person has an appointment start time of 3 days ago
        #     and they havent acted upon it, they will not see it in the front-end
        #     and wont be able to delete it, and they will not be able to
        #     add any new appointments because there's 1 thats not acted upon
        user = User.find_by_auth0_id(valid_params[:auth0_id])
        # deactivate_appointments

        user_data = user.get_attributes
        user_therapist = user.get_therapist_for_patient

        # - if the user is a patient user_data will be the patient's data
        #   and user_therapist will have a value
        # - otherwise user_data will be the therapist's data, and user_therapist
        #   will be nil
        render json: camelize({ message: 'success', user: user_data, therapist: user_therapist })
      end

      def get_therapist_for_user
        render json: camelize({ message: 'success', therapist: @user.get_therapist_for_patient })
      end

      def ios_device_token_update
        # - when the iphone sends over the device token to save it
        #   so it can send push notifications, the auth0_id pipe is
        #   replaced with %7C so we change it here
        # puts "UPDATING IOS DEVICE TOKEN"
        # puts valid_params[:apn_device_token]
        auth0_id = params[:auth0_id]
        auth0_id.sub('%7C', "|") if auth0_id.include? '%7C'
        token = valid_params[:apn_device_token]
        user = User.find_by_auth0_id(params[:auth0_id])
        user.update_attributes(apn_device_token: token)
        render json: camelize({ message: 'success', apn_device_token: token })
      end

      # - this was named like this because another method was using the name 'update'
      #   but now it can be changed back to 'update'
      def regular_update
        if @user.update_attributes(valid_params)
          render json: camelize({ message: 'success', user: @user.get_attributes })
        else
          render json: camelize({ message: 'fail' })
        end
      end

      private

      def deactivate_appointments
        # - last name is used to send the current time from the browser
        # - if the current time is past the appointment_time for any
        #   active appointments (theoretically only 1 appointment should
        #   come up) then we set is_active to false
        sec = ( valid_params[:last_name].to_f / 1000.0 )
        now_time = Time.at(sec).to_datetime
        active_appointments = @user.patient.appointments.where(is_active: true)
        active_appointments.each do |apt|
          if now_time > apt.appointment_time
            apt.update_attributes(is_deleted: true, is_active: false)
          end
        end
      end

      def valid_params
        params.require(:user).permit(
          :id,
          :auth0_id,
          :apn_device_token,
          :first_name,
          :last_name,
          :profile_photo_url,
          :accepted_disclaimer,
          :device_type,
        )
      end
    end
  end
end
