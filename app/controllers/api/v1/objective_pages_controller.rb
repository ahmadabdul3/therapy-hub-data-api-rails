# frozen_string_literal: true

module Api
  module V1
    class ObjectivePagesController < ApplicationController
      def generate_objective_page_data
        data = ObjectivePageDataGenerator.new(@user).generate
        render json: camelize({ message: 'success', data: data })
      end

      private

      # no 'valid params' yet
      # def valid_params
      #   params.require(:something).permit()
      # end
    end
  end
end
