# frozen_string_literal: true

module Api
  module V1
    class TimelineEntriesController < ApplicationController
      def index
        render json: camelize({
          message: 'success', timeline_entries: formatted_entries
        })
      end

      def show_for_user
        user = User.find(params[:user_id])
        render json: camelize({
          message: 'success', timeline_entries: user.timeline_entries_camelized
        })
      end

      # - a log entry is a hash
      def update_log
        # - append to log will save the updated log
        #   OR raise an exception if it fails
        # - we will rescue from the exception here
        entry.update_log(params[:timeline_entry][:log])
        render json: camelize({ message: 'success', log: entry.log })
      end

      private

      def valid_params
        # params.require(:timeline_entry).permit(
        #   :id,
        #   :log,
        # )
        params.require(:timeline_entry).permit!
      end

      def formatted_entries
        # - we want the latest entry first
        @formatted_entries ||= @user.timeline_entries_camelized
      end

      def entry
        return @entry if @entry
        @entry = @user.timeline_entry_for_today || TimelineEntry.new(user: @user)
      end
    end
  end
end
