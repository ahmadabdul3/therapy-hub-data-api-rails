# frozen_string_literal: true

module Api
  module V1
    class AssignmentsController < ApplicationController
      # - generally assignments are fetched for a user
      def index
        assignments = @patient.assignments.order("created_at DESC")
        camelized_assignments = assignments.map do |assignment|
          camelize(assignment.attributes)
        end
        render json: { message: 'success', assignments: camelized_assignments }
      end

      def create
        # - this is gonna change a lot because each assignment
        #   is dependant on what the result of the assignment is
        #
        # - an assignment to track a goal can have the value/text of the goal
        #   to track, but an assignment to fill in the daily status
        #   will probably not have the value of what to fill in for the
        #   daily status
        #
        # - so there will be different services that will manipulate
        #   the metadata of the assignment to make sure all the right
        #   info is there
        assignment = Assignment.new(valid_params.merge({ therapist: @therapist }))
        # do other stuff to assignment...
        # ..
        # .
        if assignment.save
          render json: camelize({ message: 'success', assignment: assignment.attributes })
        else
          render json: { message: 'fail' }
        end
      end

      def update
        assignment = Assignment.find_by_id(valid_params[:id])
        assignment.update_attributes(valid_params)

        render json: camelize({ message: 'success', assignment: assignment.attributes })
      end

      # - this is used when a therapist clicks on a patient
      #   and gets the data for that patient
      #   in that scenario, the user is a therapist
      #   so we need to get the right patient for that therapist
      #   based on the id of the patient's user
      def for_user
        assignments = User.find_by_id(params[:user_id]).patient.assignments
        render json: camelize({ message: 'success', assignments: assignments })
      end

      private

      def valid_params
        params.require(:assignment).permit!
      end
    end
  end
end
