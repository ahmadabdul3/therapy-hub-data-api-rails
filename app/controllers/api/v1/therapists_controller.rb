# frozen_string_literal: true

module Api
  module V1
    class TherapistsController < ApplicationController
      def update
        therapist = @user.therapist
        
        if therapist.update_attributes(valid_params)
          render json: camelize({ message: 'success', therapist: therapist.attributes })
        else 
          render json: camelize({ message: 'fail' })
        end
      end

      private

      def valid_params
        params.require(:therapist).permit(
          :id,
          :bio
        )
      end
    end
  end
end
