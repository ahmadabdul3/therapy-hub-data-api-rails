class Patient < ApplicationRecord
  belongs_to :therapist
  belongs_to :user
  has_many :appointments
  has_many :reviews
  has_many :assignments

  def all_appointments
    appointments.where(is_deleted: [false, nil]).order(appointment_time: :asc).map do |appointment|
      attributes = appointment.attributes
      attributes.merge({ user: therapist_user_for_appointment(appointment) })
    end
  end

  def next_appointment
    appointment = appointments.where(is_deleted: [false, nil]).order(appointment_time: :asc).first
    appointment.attributes.merge({ user: therapist_user_for_appointment(appointment) }) if appointment
  end

  private

  def therapist_user_for_appointment(appointment)
    appointment.therapist.user.attributes
  end
end
