# frozen_string_literal: true

# - we want to maintain a single list of emotional catalysts
#   this way, when a new one is added, and given as a suggestion
#   a user doesnt create a new emotional catalyst with the same
#   name, but a user will just have a new reference to the same
#   emotional catalyst through the join table
class EmotionalCatalyst < ApplicationRecord
  # relationships
  has_many :user_emotional_catalysts

  # scopes

  # - the idea here is, a suggestion can appear for a user
  #   as long as the user doesnt already have an emotional catalyst
  #   with the given text
  # - there might be a better way to do this by looking at the join table
  #   but this will do for now
  scope :suggested_happy, lambda { |text_list|
    where.not(text: text_list)
    .where(sentiment: 'positive')
    .order("RANDOM()").limit(4)
  }

  scope :suggested_unhappy, lambda { |text_list|
    where.not(text: text_list)
    .where(sentiment: 'negative')
    .order("RANDOM()").limit(4)
  }
end
