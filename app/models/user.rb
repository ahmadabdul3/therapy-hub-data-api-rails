# frozen_string_literal: true

class User < ApplicationRecord
  # relationships
  has_many :statuses
  has_many :goals
  has_many :user_emotional_catalysts
  has_many :timeline_entries
  has_one :therapist
  has_one :patient

  # read the emotional catalyst model for details on why
  # this is set up as a many to many relationship
  has_many :emotional_catalysts, :through => :user_emotional_catalysts

  serialize :roles, Array

  # callbacks
  after_create :couple_with_role, unless: :manual_create
  after_create :enroll_in_text_therapy_plan, :match_to_therapist, if: :user_was_invited

  # - the user attributes should always include the related 'patient'
  #   or 'therapist' attributes. If the user is a patient, we attach patient
  #   attributes, if the user is a therapist, we attach therapist attributes
  def get_attributes
    return self.attributes.merge(patient.attributes) if patient
    return self.attributes.merge(therapist.attributes) if therapist
    self.attributes
  end

  def get_therapist_for_patient
    patient.therapist.user.get_attributes if is_patient && patient.therapist
  end

  # - when a new therapist is created, we want to save
  #   their degree to the associated therapist object
  #   not on the user object (for now)
  def self.get_new(user_params, options=nil)
    @@degree = options[:degree]
    @@code = options[:code]
    @@manual_create = options[:manual_create]
    User.new(user_params)
  end

  # - this is used for seeds for now
  def manual_create
    @@manual_create
  end

  def hasPaidBillingStatus
    paid_billing_status = ['weekly-text', 'weekly-text-trial-paid']
    return paid_billing_status.include? billing_status
  end

  def assignPaidBillingStatus
    # - for now, this only applies to users who were on a trial.
    #   When the trial ends, and they've added a card to their account,
    #   they get switched over to 'paid' status for the trial plan
    # - users who originally started with the non-trial plan
    #   will have a billing status of 'weekly-text' to begin with
    #   so no need to update their billing status
    update_attributes(billing_status: 'weekly-text-trial-paid')
  end

  def revert_to_free_account
    return if billing_status == 'free' && patient.therapist == nil
    num_patients = patient.therapist.num_patients - 1
    update_attributes(billing_status: 'free')
    patient.therapist.update_attributes(num_patients: num_patients, accepting_patients: true)
    patient.update_attributes(therapist_id: nil)
  end

  def historical_goals
    goals.where(status: ['abandoned', 'accomplished'])
  end

  def in_progress_goals
    goals.where(status: 'in-progress')
  end

  def happy_items
    EmotionalCatalyst
      .joins(:user_emotional_catalysts)
      .where(
        sentiment: 'positive',
        user_emotional_catalysts: {
          user: self,
          status: ['active', nil]
        }
      )
  end

  def unhappy_items
    EmotionalCatalyst
      .joins(:user_emotional_catalysts)
      .where(
        sentiment: 'negative',
        user_emotional_catalysts: {
          user: self,
          status: ['active', nil]
        }
      )
  end

  def timeline_entry_for_today
    timeline_entries.where("created_at >= ?", Time.zone.now.beginning_of_day).first
  end

  def timeline_entries_camelized
    timeline_entries.order(created_at: :desc).each do |entry|
      entry.log = SnakeToCamelCaseTransformer.new(entry.log).transform
    end
  end

  def is_patient
    roles.include?('patient')
  end

  def is_therapist
    roles.include?('therapist')
  end

  def subscribe_to_weekly_text(card_source)
    plan = PatientBilling::WeeklyTextNoTrial.new(user: self, card_source: card_source)
    plan.subscribe
    match_to_therapist
  end

  def match_to_therapist
    if is_patient
      therapist = find_appropriate_therapist
      therapist.increment_num_patients
      patient.therapist = therapist
      patient.save
    end
  end

  private

  def couple_with_role
    couple = ''
    if is_patient
      couple = Patient.new(user: self)
    elsif is_therapist
      couple = Therapist.new(user: self, degree: @@degree)
    end

    couple.save
  end

  # TODO
  # - the whole matching and enrollment should happen as a separate process
  #   because when they fail, the user isnt created, which is bad
  #   we'd rather have the user be created as a free account and they can add
  #   payment later instead of the whole thing failing
  def enroll_in_text_therapy_plan
    if is_patient
      plan = PatientBilling::WeeklyTextWithTrial.new(user: self)
      plan.subscribe
    end
  end

  def static_code
    @@code ||= nil
  end

  def user_was_invited
    self.code
  end

  def find_appropriate_therapist
    # - the patient is given the therapist's unique identifier
    #   in the invitation email so they can be matched correctly
    therapist = Therapist.find_by_unique_identifier(self.code) if self.code
    return therapist if therapist

    therapist = Therapist.joins(:user)
                         .where('therapists.accepting_patients' => true, 'users.state' => state)
                         .order("RANDOM()")
                         .first
    return therapist if therapist

    Therapist.where(accepting_patients: true, is_fallback: true).order("RANDOM()").first
  end
end
