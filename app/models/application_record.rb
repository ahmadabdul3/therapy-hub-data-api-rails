class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end

#
# use case:
#
# - a user may belong to a clinic and have a therapist through their clinic,
#   but wants to have a separate therapist. In other words, they want to pay for the service
#   and use the app outside of the scope of their clinic
#   so maybe they can have many therapists?
#
# - should we allow this?
# - should it be one account with many profiles?
#   and the account info can be shared between profiles?
# - should we have a settings page where they can opt-out of being associated to the clinic
#   and pay for the service themselves - the easier way*
#
# - how do we allow users to sign up for their clinic?
# - 1. invitation emails are one way, or generating invitation code that can be used in person.
# - 2. clinic can manually add users with an email adress and a password is auto generated
# - 3. user can sign up and select if they belong to a clinic and put the clinic name
#   or the clinic can provide some sort of invitation code that they can use. same as option 1
# user = {
#   roles: [
#     'individual',
#     'belongs to clinic, means they dont pay, but the clinic pays'
#   ],
#   references: {
#     therapist: {
#       number: '1 || many',
#       notes: 'not required, users can use some free features on the site (maybe)'
#     }
#   }
# }
#
# therapist = {
#   roles: [
#     'individual',
#     'belongs to clinic, means we dont have to pay them, because the clinic pays a subscription'
#   ],
#   references: {
#     user: many
#   }
# }

# - clinics can keep associations between therapists and users (in another table?)
#
#   userTherapistTable
#   user_id  |  therapist_id  |  clinic_id (null is fine - if clinic_id is present, this user/therapist can communicate for free)
#   maybe too complicated...
#
#   if a user has an association to a therapist in the clinic,
#   that means they can access this therapist for free
#
# - otherwise, they have to pay for any other therapist they want to connect with
# clinic = {
#   roles: [
#     'free access, for ealy stage clinics',
#     '... other roles'
#   ],
#   references: {
#     user: many,
#     therapist: many
#   }
# }
