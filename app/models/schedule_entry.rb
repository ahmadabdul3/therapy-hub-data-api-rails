class ScheduleEntry < ApplicationRecord
  class DuplicateScheduleEntry < StandardError; end

  # relationships
  belongs_to :therapist

  # callbacks
  before_create :set_appointment_time, :set_day

  def self.repeating
    'repeating'
  end

  def self.one_time
    'oneTime'
  end

  # - we just check the day and the start time
  #   if 2 events are on the same day, and they start at the same
  #   time, then the times are overlapping so its duplicate
  def self.unique(params, therapist_id)
    if params[:regularity] == ScheduleEntry.repeating
      duplicate = ScheduleEntry.where(
        day: params[:day],
        start_time: params[:start_time],
        start_time_minute: params[:start_time_minute],
        start_time_period_of_day: params[:start_time_period_of_day],
        therapist_id: therapist_id
      ).first
    elsif params[:regularity] == ScheduleEntry.one_time
      duplicate = ScheduleEntry.where(
        month: params[:month],
        day_number: params[:day_number],
        year: params[:year],
        start_time: params[:start_time],
        start_time_minute: params[:start_time_minute],
        start_time_period_of_day: params[:start_time_period_of_day],
        therapist_id: therapist_id
      ).first
    end

    raise DuplicateScheduleEntry if duplicate
    ScheduleEntry.new(params)
  end

  private

  def set_appointment_time
    return if regularity == ScheduleEntry.repeating
    datestring = "#{day_number} #{month[0,3]} #{year} #{get_start_time}"
    self.appointment_time = DateTime.parse(datestring)
  end

  def get_start_time
    start_time + ':00' if start_time_period_of_day == 'am'
    "#{start_time.to_f + 12}"[0,2] + ":00"
  end
  
  def set_day
    return if regularity == ScheduleEntry.repeating
    self.day = self.appointment_time.strftime("%A") if !self.day
  end
end
