class Appointment < ApplicationRecord
  belongs_to :patient
  belongs_to :therapist

  # callbacks
  before_create :set_appointment_time, :set_day

  private

  def set_appointment_time
    datestring = "#{number} #{month[0,3]} #{year} #{get_start_time}"
    self.appointment_time = DateTime.parse(datestring)
  end

  def get_start_time
    start_time + ':00' if start_time_period_of_day == 'am'
    "#{start_time.to_f + 12}"[0,2] + ":00"
  end

  def set_day
    self.day = self.appointment_time.strftime("%A") if !self.day
  end
end
