# frozen_string_literal: true

class TimelineEntry < ApplicationRecord
  # relationships
  belongs_to :user

  # callbacks
  after_initialize :set_up_model_defaults

  # - a log entry is a hash
  #   the logic here is simple:
  #   - if the key already exists,
  #     add to it (some values are hashes, others arrays)
  #   - if the key doesnt exist, create it, then assign it the value
  #
  # - this method will perform a save
  # - since each log entry has a different structure
  #   there will be a different service for each type of
  #   log key that performs the actual update
  def update_log(log_entry)
    log_entry_type = get_log_entry_type(log_entry)
    # there should theoretically only be 1 key
    log_entry_key = log_entry.keys.first
    if log[log_entry_key]
      # append to key if it exists
      log_entry_type.append
    else
      log_entry_type.create
    end

    save
  end

  private

  def get_log_entry_type(log_entry)
    case log_entry.keys.first
    when :goals, 'goals'
      return Timeline::GoalLog.new(log, log_entry)
    when :happy_items, 'happy_items'
      return Timeline::HappyItemsLog.new(log, log_entry)
    when :unhappy_items, 'unhappy_items'
      return Timeline::UnhappyItemsLog.new(log, log_entry)
    when :status, 'status'
      return Timeline::StatusLog.new(log, log_entry)
    when :assignments, 'assignments'
      return Timeline::AssignmentLog.new(log, log_entry)
    else
      return nil
    end
  end

  def set_up_model_defaults
    self.log ||= {}
    today = DateTime.now
    self.day ||= today.strftime("%A")[0..2]
    self.month ||= today.strftime("%B")
    self.year ||= today.year.to_s
    self.number ||= today.day.to_s
  end
end

# {
#   month: 'April',
#   dayEntries: [
#     {
#       day: 'Thu',
#       number: 17,
#       events: {
#         stageOfChange: {
#           label: 'Stage of Change',
#           value: 4,
#         },
#       }
#     },
#     {
#       day: 'Wed',
#       number: 16,
#       events: {
#         stageOfChange: {
#           label: 'Stage of Change',
#           value: 4,
#         },
#         feeling: {
#           label: 'Feeling',
#           value: 'Positive',
#         },
#         thoughts: {
#           label: 'Thoughts',
#           value: 'Worked out with the help of the apps auto-scheduler',
#         },
#         goals: {
#           label: 'Added goals',
#           value: [
#             {
#               value: 'Exercise twice a week',
#               status: 'inProgress',
#             },
#             {
#               value: 'eat healthy',
#               status: 'inProgress',
#             }
#           ],
#         },
#         happyItems: {
#           label: 'Added happy items',
#           value: ['Exercise', 'Eating healthy'],
#         }
#       },
#     },
#   ]
# }
