# frozen_string_literal: true

class Goal < ApplicationRecord
  # relationships
  belongs_to :user
  belongs_to :assignment

  # validations
  # none yet

  # callbacks

  # scopes

  # - this will probably become smarter and
  #   factor in similar conditions, ages, etc in the future
  scope :suggested, lambda { |text_list|
    where.not(text: text_list).order("RANDOM()").limit(3)
  }

  def is_done?
    status == 'abandoned' || status == 'accomplished'
  end

  def is_assignment?
    assignment_id != nil
  end
end
