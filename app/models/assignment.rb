class Assignment < ApplicationRecord
  belongs_to :patient
  belongs_to :therapist
  has_one :goal

  serialize :metadata, Hash
end
