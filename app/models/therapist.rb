class Therapist < ApplicationRecord
  # relationships
  belongs_to :user
  has_many :patients
  has_many :schedule_entries
  has_many :appointments
  has_many :reviews

  # callbacks
  before_create :assign_default_values

  def increment_num_patients
    self.num_patients = self.num_patients + 1
    self.accepting_patients = false if self.num_patients == self.max_num_patients
    save
  end

  def all_appointments
    appointments.where(is_deleted: [false, nil]).order(appointment_time: :asc).map do |appointment|
      attributes = appointment.attributes
      attributes.merge({ user: patient_user_for_appointment(appointment) })
    end
  end

  def next_appointment
    appointment = appointments.where(is_deleted: [false, nil]).order(appointment_time: :asc).first
    appointment.attributes.merge({ user: patient_user_for_appointment(appointment) }) if appointment
  end

  private

  def patient_user_for_appointment(appointment)
    appointment.patient.user.attributes
  end

  def assign_default_values
    self.num_patients = 0 unless self.num_patients
    self.max_num_patients = 0 unless self.max_num_patients
    self.accepting_patients = false unless self.accepting_patients
    self.unique_identifier = SecureRandom.hex(5)
  end
end
