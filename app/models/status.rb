# frozen_string_literal: true

class Status < ApplicationRecord
  # relationships
  belongs_to :user

  # validations
  # none yet

  # callbacks
  before_create :set_default_latest_for_day

  # scopes

  # - a person can create more than one status every day, we just show the latest one
  #   so we want to pull back the record where 'latest_for_day' is true
  #   and created_at is today's date
  scope :latest_today, lambda { |user_id|
    where('created_at >= ?', Time.zone.now.beginning_of_day)
    .where(
      latest_for_day: true,
      user_id: user_id
    )
  }

  private

  def set_default_latest_for_day
    self.latest_for_day ||= true
  end
end
