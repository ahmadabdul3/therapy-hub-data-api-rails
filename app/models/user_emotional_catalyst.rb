# frozen_string_literal: true

class UserEmotionalCatalyst < ApplicationRecord
  belongs_to :user
  belongs_to :emotional_catalyst
end
