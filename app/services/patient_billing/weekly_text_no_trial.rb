module PatientBilling
  class WeeklyTextNoTrial
    def initialize(options)
      @user = options[:user]
      @card_source = options[:card_source]
      @stripe_customer = nil
      @stripe_plan = "weekly-text"
      @coupon = 'first-week-half-off'
      Stripe.api_key = ENV['STRIPE_API_KEY']
    end

    def subscribe
      populate_stripe_customer
      update_customer_card unless customer_has_card
      subscribe_customer_to_plan
    end

    def unsubscribe

    end

    private

    def populate_stripe_customer
      if @user.stripe_customer_id
        fetch_stripe_customer
      else
        create_stripe_customer
      end
    end

    def fetch_stripe_customer
      @stripe_customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
    end

    def create_stripe_customer
      @stripe_customer = Stripe::Customer.create(
        email: @user.email,
        source: @card_source
      )
      @user.update_attributes(stripe_customer_id: @stripe_customer.id)
    end

    def customer_has_card
      @stripe_customer[:sources][:data].any?
    end

    def update_customer_card
      @stripe_customer.source = @card_source
      @stripe_customer.save
    end

    def subscribe_customer_to_plan
      unless stripe_customer_subscribed_to_plan
        Stripe::Subscription.create(
          customer: @user.stripe_customer_id,
          plan: @stripe_plan
        )

        @user.update_attributes(billing_status: 'weekly-text')
      end
    end

    def stripe_customer_subscribed_to_plan
      customer_subscriptions = @stripe_customer[:subscriptions][:data]

      customer_subscriptions.each do |subscription|
        subscription_name = subscription[:plan][:id]
        return true if subscription_name == @stripe_plan
      end

      false
    end
  end
end
