module PatientBilling
  class WeeklyTextWithTrial
    def initialize(options)
      @user = options[:user]
      @card_source = options[:card_source]
      @stripe_customer = nil
      # @stripe_plan = "weekly-text-with-trial" - old 2 week plan
      @stripe_plan = "weekly-text-with-trial-1-week"
      @coupon = 'first-week-half-off'
      Stripe.api_key = ENV['STRIPE_API_KEY']
    end

    def subscribe
      populate_stripe_customer
      subscribe_customer_to_plan
    end

    def unsubscribe

    end

    private

    def populate_stripe_customer
      if @user.stripe_customer_id
        fetch_stripe_customer
      else
        create_stripe_customer
      end
    end

    def fetch_stripe_customer
      @stripe_customer = Stripe::Customer.retrieve(@user.stripe_customer_id)
    end

    def create_stripe_customer
      customer_props = { email: @user.email }
      customer_props[:source] = @card_source if @card_source
      @stripe_customer = Stripe::Customer.create(customer_props)
      @user.update_attributes(stripe_customer_id: @stripe_customer.id)
    end

    def subscribe_customer_to_plan
      unless stripe_customer_subscribed_to_plan
        Stripe::Subscription.create(
          customer: @user.stripe_customer_id,
          plan: @stripe_plan
        )

        # - for now, when the user record is subscribed to the plan
        #   it's marked as having used the trial
        user_attribs = { trial_used: true, billing_status: 'weekly-text-trial-no-card' }
        user_attribs[:billing_status] = 'weekly-text-trial-with-card' if @card_source
        @user.update_attributes(user_attribs)
      end
    end

    def stripe_customer_subscribed_to_plan
      customer_subscriptions = @stripe_customer[:subscriptions][:data]

      customer_subscriptions.each do |subscription|
        subscription_name = subscription[:plan][:id]
        return true if subscription_name == @stripe_plan
      end

      false
    end
  end
end
