# frozen_string_literal: true

class JwksHashCache
  @@jwks_hash = nil
  @@hash_timestamp = DateTime.now

  def self.update_hash(hash)
    @@hash_timestamp = DateTime.now
    @@jwks_hash = hash
  end

  def self.get_hash
    return nil if time_difference > (10 * 60 * 60)
    @@jwks_hash
  end

  def self.time_difference
    ((DateTime.now - @@hash_timestamp) * 24 * 60 * 60).to_i
  end

  def self.get_timestamp
    @@hash_timestamp
  end
end