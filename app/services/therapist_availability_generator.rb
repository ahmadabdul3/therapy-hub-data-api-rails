# frozen_string_literal: true
# - this is a big class, needs refactoring but no time right now

class TherapistAvailabilityGenerator
  def initialize(user)
    @user = user
    @day_to_num = {
      'Sunday' => 0,
      'Monday' => 1,
      'Tuesday' => 2,
      'Wednesday' => 3,
      'Thursday' => 4,
      'Friday' => 5,
      'Saturday' => 6,
    }
  end

  def generate
    ordered = order_entries
    grouped = group_entries(ordered)
    day_numbers = get_day_numbers(grouped)
    days = get_schedule_days(day_numbers)
    days_with_availability = []

    days.each do |day|
      day_name = day.strftime("%A")
      raw_entries_for_day = grouped[day_name]
      number = day.day
      month = day.strftime("%B")
      year = day.year
      entries_for_day = get_entries_for_day(raw_entries_for_day, day_name, number, month, year)
      camelized = SnakeToCamelCaseTransformer.new({ entries: entries_for_day }).transform

      if entries_for_day.any?
        day_with_availability = {
          day: day_name,
          number: number,
          month: month,
          year: year,
          available_time_frames: entries_for_day
        }
        days_with_availability << day_with_availability
      end
    end

    days_with_availability
  end

  # - entries for day are schedule entries
  #   we can have repeating entries or one-time entries
  #
  # - this method makes sure repeating entries go in all days where the
  #   day name matches, and one-time entries are only added
  #   when the right day is found
  #
  # - it also accounts for repeating or non-repeating entries that are
  #   already reserved;
  # - in other words, if I have a repeating entry for monday at 2pm-3pm
  #   it should show up every monday
  #   but if one of the mondays' 2pm-3pm entries are reserved
  #   dont show 2pm-3pm for that monday
  def get_entries_for_day(all_entries_for_day, day, number, month, year)
    final_entries_for_day = []
    number = number.to_s
    year = year.to_s
    all_entries_for_day.each do |entry|
      # - if there is an appointment that was created that has the same
      #   date values as the current entry, including the start time
      #   then dont add it
      next if appointment_exists_for_entry(entry, day, number, month, year)

      # this logic can be broken down into 2 small classes
      if entry.regularity == ScheduleEntry.repeating
        # - if its repeating, the entry wont have a day, month, and year
        #   so we should add it so its easier from the front-end to
        #   send back the right data
        entry.day = day
        entry.day_number = number
        entry.month = month
        entry.year = year
        # - adding attributes because if we add the entry
        #   then in the next round of the loop, the entry is updated with
        #   the new day, month, year, and that will update
        #   all the previous entries added (because repeating
        #   entries are the same entry record). This way
        #   a brand new object is added, and altering the entry
        #   has no effect on the old data
        final_entries_for_day << entry.attributes
      elsif entry.regularity == ScheduleEntry.one_time
        if entry.day == day && entry.day_number == number && entry.month == month && entry.year == year
          final_entries_for_day << entry.attributes
        end
      end
    end

    final_entries_for_day
  end

  def appointment_exists_for_entry(entry, day, number, month, year)
    appointments.find do |apt|
      apt.day == day && apt.number == number && apt.month == month && apt.year == year && apt.start_time == entry.start_time && apt.start_time_minute == entry.start_time_minute && apt.start_time_period_of_day == entry.start_time_period_of_day
    end
  end

  # - here we want to get all of the patient's therapist's appointments
  #   so that we can figure out which availability times are taken
  #   and not render them on the front-end to the patient viewing the
  #   availability items
  def appointments
    @appointments ||= @user.patient.therapist.appointments.where(is_deleted: [false, nil])
  end

  # - This may be used for therapists at some point
  #   to show them what their availability will look like
  def schedule_entries
    if @user.is_therapist
      return @user.therapist.schedule_entries
    elsif @user.is_patient
      return @user.patient.therapist.schedule_entries
    end
  end

  # - this orders the schedule entries from earliest to latest
  def order_entries
    entries = schedule_entries.order(:start_time_period_of_day)
    entries.order(:start_time)
  end

  # - this groups the entries by day so it looks like this:
  # { monday: [ array of schedule entries where day is monday ], tuesday: [...]}
  def group_entries(entries)
    @group ||= entries.group_by(&:day)
  end

  # - this method returns an array of all the days between 2 dates as datetime objects
  # - so it looks like this:
  # [ mon, aug 15 2017, .... ]
  def get_schedule_days(day_numbers)
    start_date = Date.today # your start
    end_date = Date.today + 1.month # your end
    my_days = day_numbers # day of the week in 0-6. Sunday is day-of-week 0; Saturday is day-of-week 6.
    result = (start_date..end_date).to_a.select {|k| my_days.include?(k.wday)}
  end

  # - this returns an array of day numbers
  # - so if the group method returns
  # { 'Monday': {...}, 'Tuesday': {..} }
  # - this will return [1, 2]
  def get_day_numbers(group)
    group.keys.map do |day|
      @day_to_num[day]
    end
  end
end
