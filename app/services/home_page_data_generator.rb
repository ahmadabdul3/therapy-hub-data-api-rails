# frozen_string_literal: true

class HomePageDataGenerator
  def initialize(user)
    @user = user
  end

  def generate
    {
      daily_status: daily_status,
      appointment: appointment,
      assignment: assignment,
    }
  end

  private

  def daily_status
    Status.latest_today(@user.id).first&.attributes
  end

  def appointment
    @user.patient.next_appointment
  end

  def assignment
    @user.patient.assignments.first&.attributes
  end
end
