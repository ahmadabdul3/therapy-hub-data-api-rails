# frozen_string_literal: true

module Timeline
  class UnhappyItemsLog
    def initialize(log, log_entry)
      @log = log
      @log_entry_key = log_entry.keys.first
      @log_entry_data = log_entry[@log_entry_key]
      @log_entry_value = @log_entry_data[:value]
      @log_entry_action = @log_entry_data[:action]
      @label = 'Unhappy Items'
    end

    def create
      @log[@log_entry_key] = {
        label: @label,
        data: [
          {
            value: @log_entry_value,
            action: @log_entry_action,
          }
        ],
      }
    end

    def append
      new_entry = { value: @log_entry_value, action: @log_entry_action }
      @log[@log_entry_key]["data"] << new_entry
    end
  end
end
