# frozen_string_literal: true

module Timeline
  class LogMutator
    def initialize(user, log_entry)
      @user = user
      @log_entry = log_entry
    end

    def mutate
      entry.update_log(@log_entry)
    end

    private

    def formatted_entries
      # - we want the latest entry first
      @formatted_entries ||= @user.timeline_entries.order(created_at: :desc)
    end

    def entry
      return @entry if @entry
      @entry = @user.timeline_entry_for_today || TimelineEntry.new
    end
  end
end
