# frozen_string_literal: true

module Timeline
  class StatusLog
    def initialize(log, log_entry)
      @log = log
      @log_entry_key = log_entry.keys.first
      @log_entry_data = log_entry[@log_entry_key]
      @log_entry_feeling = @log_entry_data[:feeling]
      @log_entry_thoughts = @log_entry_data[:thoughts]
      @label = 'Daily Status'
    end

    def create
      @log[@log_entry_key] = {
        label: @label,
        data: [
          {
            feeling: @log_entry_feeling,
            thoughts: @log_entry_thoughts,
          }
        ],
      }
    end

    def append
      new_entry = { feeling: @log_entry_feeling, thoughts: @log_entry_thoughts }
      @log[@log_entry_key]["data"] << new_entry
    end
  end
end
