# frozen_string_literal => true

class SnakeToCamelCaseTransformer
  def initialize(hash)
    @hash = hash.with_indifferent_access
  end

  def transform
    transform_keys(@hash)
  end

  private

  def transform_keys(hash)
    temp_hash = {}
    hash.keys.each do |key|
      value = hash[key]
      # if the value is a hash
      # first transform its keys to camelCase
      # then set it to the temp hash
      value = transform_keys(value) if value.is_a?(Hash)
      value = transform_array(value) if value.is_a?(Array)
      temp_hash[key.camelize(:lower)] = value
    end
    temp_hash
  end

  def transform_array(array)
    array.map do |value|
      value = transform_keys(value) if value.is_a?(Hash)
      value = transform_array(value) if value.is_a?(Array)
      value
    end
  end

  def test_hash
    {
      one_one: 'one',
      two_two: {
        three_three: 'three'
      },
      four_four: {
        five_five: {
          six_six: 'six'
        },
      },
      seven_seven: [
        {
          eight_eight: 'eight',
          nine_nine: 'nine',
        },
        {
          ten_ten: 'ten',
          eleven_eleven: 'eleven',
        },
      ],
      twelve_twelve: [
        'thirteen',
        'fourteen',
        'fifteen',
      ],
      sixteen_sixteen: [
        [
          {
            seventeen_seventeen: 'seventeen',
            eighteen_eighteen: 'eighteen',
          },
          {
            nineteen_nineteen: 'nineteen',
            twenty_twenty: 'twenty'
          },
        ],
        [
          {
            twenty_one: 'twentyone',
            twenty_two: 'twentytwo',
          },
          {
            twenty_three: 'adsf',
            twenty_four: 'four',
          },
        ],
      ],
    }
  end
end
