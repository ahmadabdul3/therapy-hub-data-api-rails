# frozen_string_literal: true

class ObjectivePageDataGenerator
  def initialize(user)
    @user = user
  end

  def generate
    {
      goals: goals,
      happy_items: happy_items,
      unhappy_items: unhappy_items,
    }
  end

  private

  def goals
    @user.in_progress_goals.limit(3).map do |goal| 
      goal.attributes
    end
  end

  def happy_items
    @user.happy_items.limit(3).map do |happy_item|
      happy_item.attributes
    end
  end

  def unhappy_items
    @user.unhappy_items.limit(3).map do |unhappy_item|
      unhappy_item.attributes
    end
  end
end
