# Therapy-hub-data-api-rails

## Getting Started

After you have cloned this repo, run this setup script to set up your machine
with the necessary dependencies to run and test this app:

    % ./bin/setup

It assumes you have a machine equipped with Ruby, Postgres, etc. If not, set up
your machine with [this script].

[this script]: https://github.com/thoughtbot/laptop

After setting up, you can run the application using [Heroku Local]:

    % heroku local

[Heroku Local]: https://devcenter.heroku.com/articles/heroku-local

## Guidelines

Use the following guides for getting things done, programming well, and
programming in style.

* [Protocol](http://github.com/thoughtbot/guides/blob/master/protocol)
* [Best Practices](http://github.com/thoughtbot/guides/blob/master/best-practices)
* [Style](http://github.com/thoughtbot/guides/blob/master/style)

## Deploying

If you have previously run the `./bin/setup` script,
you can deploy to staging and production with:

    $ ./bin/deploy staging
    $ ./bin/deploy production

## QT stuff - quoted after brew install

This formula is keg-only, which means it was not symlinked into /usr/local,
because this is an alternate version of another formula.

(according to thoughtbot install instructions, run this after installation is complete (not the below echo command):
echo 'export PATH="$(brew --prefix qt@5.5)/bin:$PATH"' >> ~/.bashrc)

If you need to have this software first in your PATH run:
  echo 'export PATH="/usr/local/opt/qt@5.5/bin:$PATH"' >> ~/.bash_profile

For compilers to find this software you may need to set:
    LDFLAGS:  -L/usr/local/opt/qt@5.5/lib
    CPPFLAGS: -I/usr/local/opt/qt@5.5/include
For pkg-config to find this software you may need to set:
    PKG_CONFIG_PATH: /usr/local/opt/qt@5.5/lib/pkgconfig

## Rails stuff - things that I have to always look up

### Model data types

:primary_key
:string
:text
:integer
:float
:decimal
:datetime
:timestamp,
:time
:date
:binary
:boolean
:references

### Migrations, model creation, etc...

- capital letters for model name
- underscores for model attributes

example:

rails g model ModelName attribute_one:text attribute_two:text
rails g migration addAttributeThreeToModelName attribute_three:text

### Scopes

scope :most_recent, -> (limit) { order("created_at desc").limit(limit) }

### Routing

#### Create a route with dashes for the route name instead of underscores:
  - resources 'home-pages', only: [:show], :controller => :home_pages, :as => :home_pages

#### Steps to perform when creating a new environment or pushing new code:
  - check environment variables - make sure they're added so everything works right
  - double check seeds - make sure they're still working with new code
  - migrate database
