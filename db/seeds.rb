# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

timeline_seeds = [
  {
    day: 'Mon',
    number: '7',
    month: '10',
    year: '2017',
    created_at: DateTime.now - 2.days
  },
  {
    day: 'Tue',
    number: '8',
    month: '10',
    year: '2017',
    created_at: DateTime.now - 1.days
  },
  {
    day: 'Wed',
    number: '9',
    month: '10',
    year: '2017',
    created_at: DateTime.now
  },
]

# user = User.first
# if user
#   timeline_seeds.each do |seed|
#     entry = TimelineEntry.new(seed.merge({ user: user }))
#     entry.save
#   end
# end

# this is the default therapist user
u = User.get_new({}, { manual_create: true })
u.first_name = 'default'
u.last_name = 'therapist'
u.email = 'default.therapist@example.com'
u.roles = ['therapist']
u.auth0_id = 'auth0|59b19b174fe7b66e81c27ccd'
u.save

t = Therapist.new
t.user = u
t.max_num_patients = 9999
t.num_patients = 0
t.accepting_patients = true
t.save
