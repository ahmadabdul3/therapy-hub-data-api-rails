class AddStartTimeMinuteEndTimeMinuteToScheduleEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :schedule_entries, :start_time_minute, :text
    add_column :schedule_entries, :end_time_minute, :text
  end
end
