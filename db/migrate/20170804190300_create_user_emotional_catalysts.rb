class CreateUserEmotionalCatalysts < ActiveRecord::Migration[5.0]
  def change
    create_table :user_emotional_catalysts do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.references :emotional_catalyst, foreign_key: true

      t.timestamps
    end
  end
end
