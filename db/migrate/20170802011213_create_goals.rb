class CreateGoals < ActiveRecord::Migration[5.0]
  def change
    create_table :goals do |t|
      t.text :text
      t.text :status

      t.timestamps
    end
  end
end
