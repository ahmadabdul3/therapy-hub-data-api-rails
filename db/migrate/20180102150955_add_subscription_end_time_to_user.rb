class AddSubscriptionEndTimeToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :subscription_end_time, :datetime
  end
end
