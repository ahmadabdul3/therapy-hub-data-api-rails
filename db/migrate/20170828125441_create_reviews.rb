class CreateReviews < ActiveRecord::Migration[5.0]
  def change
    create_table :reviews do |t|
      t.references :patient, foreign_key: true
      t.references :therapist, foreign_key: true
      t.text :title
      t.text :body
      t.integer :star_rating

      t.timestamps
    end
  end
end
