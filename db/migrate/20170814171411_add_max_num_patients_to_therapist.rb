class AddMaxNumPatientsToTherapist < ActiveRecord::Migration[5.0]
  def change
    add_column :therapists, :max_num_patients, :integer
    add_column :therapists, :num_patients, :integer
    add_column :therapists, :accepting_patients, :boolean
  end
end
