class AddBioToTherapist < ActiveRecord::Migration[5.0]
  def change
    add_column :therapists, :bio, :text
  end
end
