class AddAssignmentToGoals < ActiveRecord::Migration[5.0]
  def change
    add_reference :goals, :assignment, foreign_key: true
  end
end
