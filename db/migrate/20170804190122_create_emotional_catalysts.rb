class CreateEmotionalCatalysts < ActiveRecord::Migration[5.0]
  def change
    create_table :emotional_catalysts do |t|
      t.text :text
      t.text :sentiment

      t.timestamps
    end
  end
end
