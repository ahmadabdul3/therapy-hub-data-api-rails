class AddCityAndStateToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :city, :text
    add_column :users, :state, :text
  end
end
