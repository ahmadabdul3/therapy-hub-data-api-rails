class AddStartedByTherapistToAppointments < ActiveRecord::Migration[5.0]
  def change
    add_column :appointments, :started_by_therapist, :boolean
  end
end
