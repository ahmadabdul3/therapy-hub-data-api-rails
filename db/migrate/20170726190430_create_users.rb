class CreateUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :users, id: :uuid do |t|
      t.text :first_name
      t.text :last_name
      t.text :profile_photo_url
      t.date :dob
      t.text :email

      t.timestamps
    end
  end
end
