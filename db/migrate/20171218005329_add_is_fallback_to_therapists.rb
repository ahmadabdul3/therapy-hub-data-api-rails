class AddIsFallbackToTherapists < ActiveRecord::Migration[5.0]
  def change
    add_column :therapists, :is_fallback, :bool
  end
end
