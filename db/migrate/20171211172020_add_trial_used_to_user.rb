class AddTrialUsedToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :trial_used, :boolean, :default => false
  end
end
