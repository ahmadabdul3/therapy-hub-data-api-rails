class AddYearToTimelineEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :timeline_entries, :year, :text
  end
end
