class AddAcceptedDisclaimerToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :accepted_disclaimer, :bool
  end
end
