class AddUserToGoals < ActiveRecord::Migration[5.0]
  def change
    add_reference :goals, :user, type: :uuid, foreign_key: true
  end
end
