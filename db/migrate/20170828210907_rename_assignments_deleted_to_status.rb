class RenameAssignmentsDeletedToStatus < ActiveRecord::Migration[5.0]
  def change
    rename_column :assignments, :deleted, :status
    change_column :assignments, :status, :text
  end
end
