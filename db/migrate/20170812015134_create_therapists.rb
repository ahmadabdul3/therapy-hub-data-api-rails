class CreateTherapists < ActiveRecord::Migration[5.0]
  def change
    create_table :therapists do |t|
      t.references :user, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
