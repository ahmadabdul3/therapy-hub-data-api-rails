class CreateStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :statuses do |t|
      t.text :feeling
      t.text :thoughts
      t.boolean :latest_for_day
      t.references :user, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
