class AddBillingStatusToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :billing_status, :text, :default => 'free'
  end
end
