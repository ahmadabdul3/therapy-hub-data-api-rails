class CreateTimelineEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :timeline_entries do |t|
      t.references :user, type: :uuid, foreign_key: true
      t.jsonb :log

      t.timestamps
    end
  end
end
