class CreateAppointments < ActiveRecord::Migration[5.0]
  def change
    create_table :appointments do |t|
      t.references :patient, foreign_key: true
      t.references :therapist, foreign_key: true
      t.text :day
      t.text :number
      t.text :month
      t.text :year

      t.timestamps
    end
  end
end
