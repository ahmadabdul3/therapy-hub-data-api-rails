class AddUniqueIdentifierToTherapist < ActiveRecord::Migration[5.0]
  def change
    add_column :therapists, :unique_identifier, :text
  end
end
