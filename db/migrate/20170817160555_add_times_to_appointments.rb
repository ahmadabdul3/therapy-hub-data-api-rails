class AddTimesToAppointments < ActiveRecord::Migration[5.0]
  def change
    add_column :appointments, :start_time, :text
    add_column :appointments, :start_time_period_of_day, :text
    add_column :appointments, :end_time, :text
    add_column :appointments, :end_time_period_of_day, :text
  end
end
