class AddAuth0IdToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :auth0_id, :text
  end
end
