class AddIsPaidToAppointment < ActiveRecord::Migration[5.0]
  def change
    add_column :appointments, :is_paid, :bool
  end
end
