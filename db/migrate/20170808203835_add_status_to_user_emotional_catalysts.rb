class AddStatusToUserEmotionalCatalysts < ActiveRecord::Migration[5.0]
  def change
    add_column :user_emotional_catalysts, :status, :text
  end
end
