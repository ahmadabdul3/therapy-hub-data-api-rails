class CreateScheduleEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :schedule_entries do |t|
      t.text :day
      t.text :regularity
      t.datetime :start_date
      t.datetime :end_date
      t.references :therapist, foreign_key: true
      t.text :start_time
      t.text :start_time_period_of_day
      t.text :end_time
      t.text :end_time_period_of_day
      t.text :month
      t.text :day_number
      t.text :year

      t.timestamps
    end
  end
end
