class AddApnDeviceTokenToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :apn_device_token, :text
  end
end
