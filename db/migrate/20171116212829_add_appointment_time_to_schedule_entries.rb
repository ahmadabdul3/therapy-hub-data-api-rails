class AddAppointmentTimeToScheduleEntries < ActiveRecord::Migration[5.0]
  def change
    add_column :schedule_entries, :appointment_time, :datetime
  end
end
