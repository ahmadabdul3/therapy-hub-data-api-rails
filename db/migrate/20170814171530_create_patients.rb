class CreatePatients < ActiveRecord::Migration[5.0]
  def change
    create_table :patients do |t|
      t.references :therapist, foreign_key: true
      t.references :user, type: :uuid, foreign_key: true

      t.timestamps
    end
  end
end
