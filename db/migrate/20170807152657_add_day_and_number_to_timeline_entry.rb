class AddDayAndNumberToTimelineEntry < ActiveRecord::Migration[5.0]
  def change
    add_column :timeline_entries, :day, :text
    add_column :timeline_entries, :number, :text
  end
end
