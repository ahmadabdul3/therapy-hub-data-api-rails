class AddStartTimeMinuteEndTimeMinuteToAppointments < ActiveRecord::Migration[5.0]
  def change
    add_column :appointments, :start_time_minute, :text
    add_column :appointments, :end_time_minute, :text
  end
end
