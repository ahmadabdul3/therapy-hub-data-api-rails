class AddIsDeletedToAppointment < ActiveRecord::Migration[5.0]
  def change
    add_column :appointments, :is_deleted, :boolean
  end
end
