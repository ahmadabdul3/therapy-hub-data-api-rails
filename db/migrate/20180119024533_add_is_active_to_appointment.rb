class AddIsActiveToAppointment < ActiveRecord::Migration[5.0]
  def change
    add_column :appointments, :is_active, :boolean, :default => true
  end
end
