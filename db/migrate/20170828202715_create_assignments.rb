class CreateAssignments < ActiveRecord::Migration[5.0]
  def change
    create_table :assignments do |t|
      t.references :patient, foreign_key: true
      t.references :therapist, foreign_key: true
      t.text :note
      t.text :context
      t.jsonb :metadata
      t.boolean :deleted

      t.timestamps
    end
  end
end
