class CreatePatientInvitations < ActiveRecord::Migration[5.0]
  def change
    create_table :patient_invitations do |t|
      t.text :email
      t.references :therapist, foreign_key: true

      t.timestamps
    end
  end
end
