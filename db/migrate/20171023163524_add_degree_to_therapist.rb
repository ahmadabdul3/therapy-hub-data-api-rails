class AddDegreeToTherapist < ActiveRecord::Migration[5.0]
  def change
    add_column :therapists, :degree, :text
  end
end
