# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180214135944) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "uuid-ossp"

  create_table "appointments", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "therapist_id"
    t.text     "day"
    t.text     "number"
    t.text     "month"
    t.text     "year"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.text     "start_time"
    t.text     "start_time_period_of_day"
    t.text     "end_time"
    t.text     "end_time_period_of_day"
    t.datetime "appointment_time"
    t.text     "start_time_minute"
    t.text     "end_time_minute"
    t.boolean  "is_paid"
    t.boolean  "is_deleted"
    t.boolean  "is_active",                default: true
    t.boolean  "started_by_therapist"
    t.index ["patient_id"], name: "index_appointments_on_patient_id", using: :btree
    t.index ["therapist_id"], name: "index_appointments_on_therapist_id", using: :btree
  end

  create_table "assignments", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "therapist_id"
    t.text     "note"
    t.text     "context"
    t.jsonb    "metadata"
    t.text     "status"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["patient_id"], name: "index_assignments_on_patient_id", using: :btree
    t.index ["therapist_id"], name: "index_assignments_on_therapist_id", using: :btree
  end

  create_table "emotional_catalysts", force: :cascade do |t|
    t.text     "text"
    t.text     "sentiment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "goals", force: :cascade do |t|
    t.text     "text"
    t.text     "status"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.uuid     "user_id"
    t.integer  "assignment_id"
    t.index ["assignment_id"], name: "index_goals_on_assignment_id", using: :btree
    t.index ["user_id"], name: "index_goals_on_user_id", using: :btree
  end

  create_table "patient_invitations", force: :cascade do |t|
    t.text     "email"
    t.integer  "therapist_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["therapist_id"], name: "index_patient_invitations_on_therapist_id", using: :btree
  end

  create_table "patients", force: :cascade do |t|
    t.integer  "therapist_id"
    t.uuid     "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["therapist_id"], name: "index_patients_on_therapist_id", using: :btree
    t.index ["user_id"], name: "index_patients_on_user_id", using: :btree
  end

  create_table "reviews", force: :cascade do |t|
    t.integer  "patient_id"
    t.integer  "therapist_id"
    t.text     "title"
    t.text     "body"
    t.integer  "star_rating"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["patient_id"], name: "index_reviews_on_patient_id", using: :btree
    t.index ["therapist_id"], name: "index_reviews_on_therapist_id", using: :btree
  end

  create_table "schedule_entries", force: :cascade do |t|
    t.text     "day"
    t.text     "regularity"
    t.datetime "start_date"
    t.datetime "end_date"
    t.integer  "therapist_id"
    t.text     "start_time"
    t.text     "start_time_period_of_day"
    t.text     "end_time"
    t.text     "end_time_period_of_day"
    t.text     "month"
    t.text     "day_number"
    t.text     "year"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "start_time_minute"
    t.text     "end_time_minute"
    t.datetime "appointment_time"
    t.index ["therapist_id"], name: "index_schedule_entries_on_therapist_id", using: :btree
  end

  create_table "statuses", force: :cascade do |t|
    t.text     "feeling"
    t.text     "thoughts"
    t.boolean  "latest_for_day"
    t.uuid     "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["user_id"], name: "index_statuses_on_user_id", using: :btree
  end

  create_table "therapists", force: :cascade do |t|
    t.uuid     "user_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "max_num_patients"
    t.integer  "num_patients"
    t.boolean  "accepting_patients"
    t.text     "bio"
    t.text     "degree"
    t.text     "unique_identifier"
    t.boolean  "is_fallback"
    t.index ["user_id"], name: "index_therapists_on_user_id", using: :btree
  end

  create_table "timeline_entries", force: :cascade do |t|
    t.uuid     "user_id"
    t.jsonb    "log"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "day"
    t.text     "number"
    t.text     "month"
    t.text     "year"
    t.index ["user_id"], name: "index_timeline_entries_on_user_id", using: :btree
  end

  create_table "user_emotional_catalysts", force: :cascade do |t|
    t.uuid     "user_id"
    t.integer  "emotional_catalyst_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.text     "status"
    t.index ["emotional_catalyst_id"], name: "index_user_emotional_catalysts_on_emotional_catalyst_id", using: :btree
    t.index ["user_id"], name: "index_user_emotional_catalysts_on_user_id", using: :btree
  end

  create_table "users", id: :uuid, default: -> { "uuid_generate_v4()" }, force: :cascade do |t|
    t.text     "first_name"
    t.text     "last_name"
    t.text     "profile_photo_url"
    t.date     "dob"
    t.text     "email"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.text     "auth0_id"
    t.jsonb    "roles"
    t.text     "apn_device_token"
    t.text     "city"
    t.text     "state"
    t.text     "billing_status",        default: "free"
    t.text     "stripe_customer_id"
    t.boolean  "trial_used",            default: false
    t.boolean  "accepted_disclaimer"
    t.datetime "subscription_end_time"
    t.string   "device_type"
    t.string   "text"
    t.text     "code"
  end

  add_foreign_key "appointments", "patients"
  add_foreign_key "appointments", "therapists"
  add_foreign_key "assignments", "patients"
  add_foreign_key "assignments", "therapists"
  add_foreign_key "goals", "assignments"
  add_foreign_key "goals", "users"
  add_foreign_key "patient_invitations", "therapists"
  add_foreign_key "patients", "therapists"
  add_foreign_key "patients", "users"
  add_foreign_key "reviews", "patients"
  add_foreign_key "reviews", "therapists"
  add_foreign_key "schedule_entries", "therapists"
  add_foreign_key "statuses", "users"
  add_foreign_key "therapists", "users"
  add_foreign_key "timeline_entries", "users"
  add_foreign_key "user_emotional_catalysts", "emotional_catalysts"
  add_foreign_key "user_emotional_catalysts", "users"
end
